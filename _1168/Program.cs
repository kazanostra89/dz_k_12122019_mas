﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1168
{
    class Program
    {
        static void Main(string[] args)
        {
            int N, r, kolichUchastn;
            N = 22;
            kolichUchastn = 0;

            Random rnd = new Random();
            int[] nas = new int[N];

            for (int i = 0; i < N; i++)
            {
                nas[i] = rnd.Next(160, 210);
            }
            /* Проверка для себя
            for (int i = 0; i < N; i++)
            {
                Console.Write(nas[i] + " ");
            }
            */
            Console.Write("\nВведите значение роста для фильтрации участников: ");
            r = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < N; i++)
            {
                if (nas[i] <= r)
                {
                    kolichUchastn = kolichUchastn + 1;
                }
            }

            Console.WriteLine("\n Количество участников при r <= " + r + " = " + kolichUchastn);

            Console.ReadKey();
        }
    }
}
