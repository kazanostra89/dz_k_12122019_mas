﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1140
{
    class Program
    {
        static void Main(string[] args)
        {
            //int[] nas = new int[] { 5, -3, 2, 8, -4 };
            int N;
            
            Console.Write("Введите размерность массива: ");
            N = Convert.ToInt32(Console.ReadLine());

            int[] nas = new int[N];

            for (int i = 0; i < N; i++)
            {
                Console.Write("Введите занчение массива nas[" + i + "] = ");
                nas[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < N; i++)
            {
                if (nas[i] >= 0)
                {
                    Console.Write(nas[i] + " ");
                }                
            }

            Console.WriteLine();

            for (int i = 0; i < N; i++)
            {
                if (nas[i] < 0)
                {
                    Console.Write(nas[i] + " ");
                }
            }

            Console.ReadLine();
        }
    }
}
